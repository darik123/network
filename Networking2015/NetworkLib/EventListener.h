#pragma once

#include <SDL.h>

class EventListener
{
public:
  virtual ~EventListener() = 0 {}

  void ListenerEvent (const SDL_Event& event);

  virtual void OnKeyboardEvent(const SDL_KeyboardEvent&) {}
  virtual void OnJoyAxisEvent(const SDL_JoyAxisEvent&) {}
  virtual void OnJoyButtonEvent(const SDL_JoyButtonEvent&) {}
  virtual void OnMouseMotionEvent(const SDL_MouseMotionEvent&) {}
  virtual void OnMouseButtonEvent(const SDL_MouseButtonEvent&) {}
  //.. include all other events
};