#include "ServerSocket.h"

#define SDL_reinterpret_cast(type, expresssion) reinterpret_cast<type>(expresssion)


Socket* ServerSocket::Accept()
{
  TCPsocket newSock = 0;
  while(!newSock)
  {
    newSock = SDLNet_TCP_Accept(m_tcpSock);
    SDL_Delay(1000);
   // std::cout << ".";
  }
  return new Socket(newSock);
}
//void ServerSocket::Accept()
//{
//  m_tcpSock = nullptr;
//  while(!m_tcpSock)
//  {
//    m_tcpSock = SDLNet_TCP_Accept(m_listenSock);
//    SDL_Delay(300);
//  }
//  SetSock(m_tcpSock);
//}

bool ServerSocket::ServerOpen(int port)
{
  IPaddress ip;
  SDLNet_ResolveHost(&ip, INADDR_ANY, port);

  if(ip.host == INADDR_NONE)
  {
    std::cout << "No host name" << "\n";
    return false;
  }

  m_tcpSock = SDLNet_TCP_Open(&ip);

  if(!m_tcpSock)
  {
    std::cout << "No server socket" << "\n";
    return false;
  }

  //SDLNet_TCP_AddSocket(m_sockSet, m_tcpSock);

  return true;
}