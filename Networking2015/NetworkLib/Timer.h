#pragma once

class Timer  // is a singleton design.
{
private:
  Timer();

  Timer(const Timer&); // copy ctor no definition!
  Timer& operator=(const Timer&); // assignment operator no definition.

public:
  static Timer* Instance(); // static infornt of a member function allowed you to call the function before you actually create a class object eg Timer Object.

  void Update();
  float GetDt() const;

private:
  float m_dt;
  float m_previousTime;
};