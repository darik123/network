#pragma once

#include "GameState.h"
#include "Singleton.h"
#include "Uncopyable.h"
#include "Image.h"
//#include "SoundManager.h"

class SplashState;
typedef Singleton<SplashState> TheSplashState;

class SplashState : public GameState
{
public:
  bool Load();

  // overrides game state functions
  virtual void Update();
  virtual void Draw();
  virtual void OnActive();
  virtual void OnInActive();

  // override function inf EventHangler
  virtual void OnKeyboardEvent(const SDL_KeyboardEvent&);

private:
  SplashState(){};
  friend TheSplashState;
  Image m_image;
};