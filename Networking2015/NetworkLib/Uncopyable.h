#pragma once
// prevents your uncopyable object being created outside the class as 
// you don't want it to be created.
// Its used with other classes to have an effect on their function
class Uncopyable
{
protected:   
  Uncopyable() {}
private:
  Uncopyable (const Uncopyable&);
  Uncopyable& operator = (const Uncopyable&);
};