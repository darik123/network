#include <SDL_Image.h>
#include <assert.h>
#include "Image.h"
#include "Screen.h"


Image::Image()
{
  m_surface = 0;
}

bool Image::Load(const std::string& fileName)
{
  m_surface = IMG_Load(fileName.c_str());  // c_str allows C to understand the C++ string function
  assert(m_surface);

  return(m_surface != 0); // if image not equal to 0 return true.
}

void Image::Draw(int x, int y)
{
  SDL_Surface* screen = TheScreen::Instance()->GetSurface();

  SDL_Rect r = { x, y, 0, 0 };
  SDL_BlitSurface(m_surface, 0, screen, &r);
}