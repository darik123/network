#include <SDL.h>
#include "Timer.h"

Timer::Timer()
{
  m_dt = 0;
  m_previousTime = 0;
}

Timer* Timer::Instance()
{
  static Timer t;
  return &t;
}

void Timer::Update()
{
  float time = (float)(SDL_GetTicks()) * 0.001f; // SDL_GetTicks gets time in millisecs. Dividing it by 1000 converts to Seconds however as divide is expensive compared to multiplying which is cheaper you can multiple by 0.001 to convert to seconds.
  m_dt = time - m_previousTime;
  m_previousTime = time;

  const float MAX_DT = 0.1f;
  if (m_dt > MAX_DT)
  {
    m_dt = MAX_DT;
  }
}

float Timer::GetDt() const
{
  return m_dt;
}
