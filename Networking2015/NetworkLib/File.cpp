#include <assert.h>

#include "File.h"

bool File::OpenRead(const std::string& filename)
{
  assert(!m_file.is_open());  // checks file is not already open

  m_file.open(filename);

  assert(m_file.is_open());  // checks file is open

  return (m_file.is_open());

}

bool File::GetString(std::string* result)
{
  assert(m_file.is_open());

  std::string s;

  while (true)
  {
    if (m_file.eof()) // if reached end of file 
    {
      return false;
    }

    std::getline(m_file, s); 

    if (s.empty()) // if blank line
    {
      continue; //restart loop
    }

    if (s[0] == '#') // if starts with # its a comment
    {
      continue; // restart loop
    }

    break;
  }
  *result = s;
  return true;
}

bool File::GetInteger(int* result)
{
  std::string s;
  if (!GetString(&s))
  {
    return false;
  }

  *result = atoi(s.c_str());
  return true;

}

bool File::GetFloat(float* result)
{
  std::string s;
  if (!GetString(&s))
  {
    return false;
  }

  *result = atof(s.c_str());
  return true;

}


