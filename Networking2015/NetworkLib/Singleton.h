#pragma once

#include "Uncopyable.h"

template <class T> // class T is a convention T meaning Type.
class Singleton : public Uncopyable
{
public:
  static T* Instance()
  {
    static T t; // t will be the instance name you will choose.
    return &t;
  }
};