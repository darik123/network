#include "GameObject.h"

void GameObject::SetId(int id)
{
  m_id = id;
}
int GameObject::GetID() const
{
  return m_id;
}

void GameObject::SetPos(const Vec2f& pos)
{
  m_pos = pos;
}
