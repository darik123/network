#include "GameState.h"
#include "EventPoller.h"

void GameState::OnActive()
{
  TheEventPoller::Instance()->AddListener(this);
}

void GameState::OnInActive()
{
  TheEventPoller::Instance()->RemoveListener(this);
}