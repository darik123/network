#pragma once

#include <SDL.h>
#include <string>

class Image
{
public:
  Image();
  bool Load(const std::string& fileName);
  void Draw(int x, int y);

protected:
  SDL_Surface* m_surface;
};