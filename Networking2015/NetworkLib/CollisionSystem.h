#pragma once

#include "GameObject.h"

bool TheyCollide(GameObject* go1, GameObject* go2);

class CollisionSystem
{
public:
  virtual ~CollisionSystem() {}

  virtual void Update(GameObjectMap*) = 0;
};