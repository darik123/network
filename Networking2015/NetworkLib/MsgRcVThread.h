#pragma once

#include "Thread.h"
#include "Mutex.h"
#include "MessageQueue.h"
#include "GMGetTurn.h"

class MsgRcVThread : public Thread
{
public:
  MsgRcVThread() {}

  MsgRcVThread(Socket* sock) : m_socket(sock) {}

  void Work()
  {
    while(true)
    {
      m_mutex.Lock();

      GameMessage* gm = new GMGetTurn;
      
      if(gm = ReceiveGameMessage(m_socket))
      {
        TheGameMessageQueue::Instance()->AddMessage(gm);
        //TheGameMessageQueue::Instance()->Update();
      }
    }
  }
private:
  Socket* m_socket;
  Mutex m_mutex;
};