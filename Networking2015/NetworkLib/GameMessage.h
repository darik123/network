#pragma once

#include <string>
#include "SocketMessage.h"
#include "Socket.h"
//#include "GameMessageFactory.h"

class GameMessage{
public:
  GameMessage()
  {
    m_type = "";
  }
  virtual ~GameMessage(){}

  virtual void Execute() = 0;

  virtual void WriteSocketMessage(SocketMessage* socketMessage) = 0;
  virtual bool ReadSocketMessage(SocketMessage* socketMessage) = 0;
/*
  void SetTimeStamp(float f);
  float GetTimeStamp() const;*/

  virtual const char* GetTypeName() const = 0;
  void SetType(std::string s)
  {
    m_type = s;
  }

  bool operator<(const GameMessage& gm)const;

protected:
  std::string m_id;
  //float m_timeStamp;
  std::string m_type;
};

GameMessage* ReceiveGameMessage(Socket* socket);
bool SendGameMessage(Socket* socket, GameMessage* gm);
