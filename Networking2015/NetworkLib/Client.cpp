#include "Client.h"

Client::Client()
{
  m_id = 0;
  //m_socket = 0;
}
int Client::GetClientId() const
{
  return m_id;
}
void Client::SetClientId(int id)
{
  m_mutex.Lock();
  m_id = id;
}
bool Client::ServerConnect()
{
  m_mutex.Lock();
  if(!m_socket->ClientOpen("localhost", 777))
  {
    std::cout << "Can't Connect" << "\n";
    return false;
  }

    return true;
}

 /* while(true)
  {
    if(m_socket->HasData())
    {
      GameMessage* gm = NULL;

      gm = RecvGameMsg(m_socket);

      return true;
    }

    if(!m_socket->HasData())
    {
      std::cout << "No Data from GM" << "\n";
    }
  }*/
//Client::~Client()
//{
//  m_socket->Close();
//}