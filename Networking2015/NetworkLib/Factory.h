#pragma once
#include <assert.h>
#include <map>
#include "Singleton.h"


template <class T>
class Factory: public Uncopyable
{
private:
	Factory(){}
	friend Singleton<Factory>;
public:
	typedef T* (*FactoryFunc)();//typedef for the pointer to a function with the name FactoryFunc

	int Add(const std::string& type, FactoryFunc ff)
  {
		m_map[type] = ff;
		static int i = 0;
		i++;
		return i;
	}

	T* Create(const std::string& type)
  {
		auto it = m_map.find(type);
		if (it == m_map.end()) return nullptr;//key is not in the map
		FactoryFunc ff = it->second;
		assert(ff);
		T* go = ff();
		assert(go);
		return go;
	}

private:
	typedef std::map<std::string, FactoryFunc> FactoryMap;
	FactoryMap m_map;
};
//#pragma once
//
//#include <map>
//#include <string>
//#include "Singleton.h"
//
//template <class T>
//class Factory
//{
//public:
//
//	typedef T* (*Function)();
//  typedef std::map<std::string, Function> FactoryMap;
//	
//	T* Create(const std::string& typeName) 
//	{
//		Function f = m_Map[typeName];
//		if(f != 0)
//		{
//			return f();
//		}
//		return NULL;
//	}
//
//
//	bool AddObject (const std::string& typeName, Function f)
//	{
//		m_Map[typeName] = f;
//		return true;	
//	}
//private:
//  Factory() {}
//  friend class Singleton<Factory>;
//	FactoryMap m_Map;
//};