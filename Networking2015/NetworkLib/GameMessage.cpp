#include "GameMessage.h"
#include "GameMessageFactory.h"
//#include "GameMessageQueue.h"

//GameMessage::GameMessage()
//{
//	m_timeStamp = 0.0f;
//}
//
//void GameMessage::SetTimeStamp(float f)
//{
//	m_timeStamp = f; 
//}
//float GameMessage::GetTimeStamp()const
//{
//	return m_timeStamp;
//}
//
//bool GameMessage::operator<(const GameMessage& gm)const
//{
//	return m_timeStamp < gm.m_timeStamp;
//}

GameMessage* ReceiveGameMessage(Socket* socket){
	//this will be called inside a threat which will alway kepp listening for messages 
	//and then pass messages to message queue
	if (socket->HasData())
  {
		SocketMessage sm;
		if (!socket->Receive(&sm))
    {
			return nullptr;
		}
		std::string GMId;
		if (!sm.ReadString(&GMId))
    {
			return nullptr;
		}
		GameMessage* gm = TheGameMessageFactory::Instance()->Create(GMId);
		if (!gm)
    {
			return nullptr;
		}
		//decode the gamemessage
		//start by reading time stamp because we have already read the string id
		if (!gm->ReadSocketMessage(&sm))
    {
			return nullptr;	
		}
		return gm;//now GM should be added to the GMQueue
	}
	else return nullptr;
}

bool SendGameMessage(Socket* socket, GameMessage* gm)
{
	SocketMessage sm;
	gm->WriteSocketMessage(&sm);

  if (!socket->Send(sm))
  {
		return false;
	}
	return true;
}