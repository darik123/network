// ALWAYS INCLUDE IN THE PREPROCESSOR!!!!!
#ifdef MEM_DEBUG //use this to turn on the code

//still needed include guards
#pragma once

#include <iostream>
#include <fstream>
#include <map>
#include "Singleton.h"

void* operator new(size_t sizeOfBytes);

void* operator new(size_t sizeOfBytes, 
  const char* filename, 
  int lineNum);

void operator delete(void* p); // pass in the pointer returned from new

struct Allocation
{
  // Allocation() : m_size(0), m_addr(0) {}

  Allocation(size_t size = 0, void* addr = 0, 
    const char* filename = "", int lineNum = 0) :
      m_size(size), m_addr(addr), m_filename(filename), m_lineNum(lineNum) 
      {}

  const char* m_filename;
  int m_lineNum;
  size_t m_size;
  void* m_addr;
};

// Use this non-member function to override the operator << 
std::ostream& operator<<(std::ostream& os, const Allocation& alloc);

class AllocMan;
typedef Singleton<AllocMan> TheAllocMan;
class AllocMan : public Uncopyable
{
public:
  ~AllocMan();

  void AddAlloc(const Allocation& a);
  void RemoveAlloc(void* addr);
  void ReportLeaks();

private:
  AllocMan() {}

  friend Singleton<AllocMan>;

  typedef std::map<void*, Allocation> AllocMap;
  AllocMap m_map;
};

// Adding to new
#define new new(__FILE__, __LINE__)

#endif // ifdef MEM_DEBUG