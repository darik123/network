#pragma once

#include "Singleton.h"
#include "Uncopyable.h"
#include "ClientSocket.h"
#include "GameMessage.h"
#include "Mutex.h"

class Client : public Uncopyable
{
public:
  Client();
  bool ServerConnect();
  //bool SendMessage(GameMessage &gm);

  int GetClientId() const;
  void SetClientId(int id);

private:
  /*~Client();*/
  ClientSocket* m_socket;
  int m_id;
  Mutex m_mutex;
};

typedef Singleton<Client> TheClient;