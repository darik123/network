#include <assert.h>
#include "SpriteSheet.h"
#include "Screen.h"

SpriteSheet::SpriteSheet()
{
  m_numCellsInX = 1;
  m_numCellsInY = 1;
  
  m_cellWidth = 0;
  m_cellHeight = 0;
}

bool SpriteSheet::Load(const std::string& fileName, int numCellsInX, int numCellsInY)
{
  m_numCellsInX = numCellsInX;
  m_numCellsInY = numCellsInY;

  if (!Image::Load(fileName))
  {
    return false;
  }

  m_cellWidth = m_surface->w / m_numCellsInX;

  //height is the height / number cells in Y
  m_cellHeight = m_surface->h / m_numCellsInY;

  return true;
}

void SpriteSheet::Draw (int x, int y, int cellNum)
{
  assert(m_surface);


  // Find top left co ord
  //X value
  int cellX = (cellNum % m_numCellsInX) * m_cellWidth;
  // value
  int cellY = (cellNum / m_numCellsInX) * m_cellHeight;

  //Draw just the cell

  SDL_Surface* screen = TheScreen::Instance()->GetSurface();

  SDL_Rect r = { x, y, 0, 0 };
  SDL_Rect area = {cellX,cellY,m_cellWidth,m_cellHeight};
  SDL_BlitSurface(m_surface, &area, screen, &r);
}