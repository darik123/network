#pragma once

#include "GameState.h"
#include "Singleton.h"
#include "GameObject.h"

class Game;
typedef Singleton<Game> TheGame;

class CollisionSystem;

typedef void (*CollisionHandler)
  (GameObject*, GameObject*);

class Game
{
public:
  void RunGame();
  void Set(GameState* state);
  void AddGameObjects(int i, GameObject* go);

  GameObject* GetGameObject (int id);
  void DeleteGameObject (int id);
  void DeleteAllGameObjects ();

  void UpdateGameObjects();
  void DrawGameObjects();
  void UpdateCollisions();
  void SetCollisionSystem(CollisionSystem* cs);

  void HandleCollision(GameObject* g1, GameObject* g2);

  bool AddCollisionHandler(
    const std::string& typeName1,
    const std::string& typeName2,
    CollisionHandler ch);


private:
  Game();

  friend TheGame;
  void Update();
  void Draw();

  typedef std::pair<std::string, std::string>
    TypePair;

  typedef std::map<TypePair, CollisionHandler>
    CollisionMap;

  CollisionMap m_collMap;

  GameState* m_state;

  GameObjectMap m_gameObjects;// global typedefed in the gameObject.h

  CollisionSystem* m_collisionSystem;
};