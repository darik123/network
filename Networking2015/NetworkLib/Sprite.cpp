#include <assert.h>
#include "Sprite.h"
#include "Timer.h"


Sprite::Sprite()
{
  m_spriteSheet = 0;
  m_elapsedTime = 0;
  m_maxElapsedTime = 0;
  m_cellNum = 0;

  m_acc.y = 400.0f ; // pixels per second per second
}

void Sprite::SetSprite(SpriteSheet* ss, float maxElaspedTime)
{
  m_spriteSheet = ss;
  m_maxElapsedTime = maxElaspedTime;
}

void Sprite::Draw()
{
  assert(m_spriteSheet);
  m_spriteSheet->Draw(m_pos.x,m_pos.y,m_cellNum);
}

void Sprite::Update()
{
  float dt = Timer::Instance()->GetDt();
  m_elapsedTime += dt;
  if (m_elapsedTime > m_maxElapsedTime)
  {
    m_elapsedTime = 0;
    m_cellNum++;

    if (m_cellNum > 0)
    {
      m_cellNum = 0;
    }
  }
  m_vel += m_acc * dt;
  m_pos += m_vel * dt;

}
AABBf Sprite::GetAABB() const
{
  int cellWidth = m_spriteSheet->GetCellWidth();
  int cellHeight = m_spriteSheet->GetCellHeight();

  return AABBf(
    m_pos.x, 
    m_pos.x + cellWidth, 
    m_pos.y, 
    m_pos.y + cellHeight
    );
}

Vec2f Sprite::Center()  // returns centerpoint coordinate.
{
  int cellWidth = m_spriteSheet->GetCellWidth();
  int cellHeight = m_spriteSheet->GetCellHeight();

  return Vec2f(
    m_pos.x + (cellWidth/2),
    m_pos.y + (cellHeight/2)
    );
}