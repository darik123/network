#pragma once
#include "GameMessage.h"

class GMGetTurn : public GameMessage{
public:
	GMGetTurn();

	virtual void Execute() override;
	virtual void WriteSocketMessage(SocketMessage* socketMessage) override;
	virtual bool ReadSocketMessage(SocketMessage* socketMessage) override;

	static const char* StaticTypeName();
	virtual const char* GetTypeName() const override;
private:
	int m_currentTurn;
};