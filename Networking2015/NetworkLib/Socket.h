#pragma once

#include <iostream>
#include "Uncopyable.h"
#include "SocketMessage.h"
#include <SDL_net.h>

class Socket
{
public:
  //Socket() : m_tcpSock(0) { m_sockSet = SDLNet_AllocSocketSet(1); }

  Socket(TCPsocket sock);

  Socket();

  virtual ~Socket();

  bool HasData() const;

  /*bool Send(const SocketMessage& sockMsg);
  bool Recv(SocketMessage* sockMsg);*/

  bool Receive(SocketMessage* sm);
  bool Send(const SocketMessage& message);

  void PushIn()
  {
    m_vec.push_back(*this);
  }

  Socket* getAt(int i)
  {
    Socket* s = nullptr;
    s = &m_vec.at(i);
    return s;
  }

  void Close();
protected:
  SDLNet_SocketSet m_sockSet;
  TCPsocket m_tcpSock;
  std::vector<Socket> m_vec;
  //int m_id;
};