#pragma once
#include <queue>
#include "GameMessage.h"
#include "Mutex.h"
#include "Singleton.h"
#include "Thread.h"
#include "Uncopyable.h"

class GameMessageQueue : public Uncopyable, public Thread
{
private:
	GameMessageQueue() {}
  ~GameMessageQueue() {}
	friend Singleton<GameMessageQueue>;
public:
	void AddMessage(GameMessage* gm);
	void Update();//execute next messages
   void Work()
  {
    while(true)
    {
      m_mutex.Lock();
      this->Update();
    }
  }
	void Clear();
private:
	std::priority_queue<GameMessage*> m_GMQueue;
  Mutex m_mutex;
};

typedef Singleton<GameMessageQueue> TheGameMessageQueue;

/*class MessageQueue : public Uncopyable, public Thread
{
public:
        virtual void Update();
        void AddToQueue(GameMessage* Gmsg);
        virtual void Work();

private:
        friend class Singleton<MessageQueue>;

        struct GmLessThan
        {
                bool operator()(GameMessage*,GameMessage*);
        };

protected:

        typedef std::priority_queue<GameMessage*,std::vector<GameMessage*>,GmLessThan> Q;
        Q m_MsgQueue;
        Mutex m_mutex;
};

typedef Singleton<MessageQueue> TheMessageQueue*/;