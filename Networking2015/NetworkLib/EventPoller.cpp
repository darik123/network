#include "EventPoller.h"

void EventPoller::Update()
{
  Listeners copy = m_listeners;
  SDL_Event event;
  while (SDL_PollEvent(&event))
  {
    if (event.type == SDL_QUIT)
    {
      exit(0);
    }

    for (Listeners::iterator it = copy.begin();
      it != copy.end(); 
      ++it)
    {
      EventListener* eh = *it;
      eh->ListenerEvent(event);
    }
  }
}

void EventPoller::AddListener(EventListener* eh)
{
  m_listeners.insert(eh);
}

void EventPoller::RemoveListener(EventListener* eh)
{
  m_listeners.erase(eh);
}