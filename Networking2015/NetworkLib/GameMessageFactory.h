#pragma once

#include "Factory.h"
#include "Singleton.h"
#include "GameMessage.h"

//typedef Singleton<Factory<GameMessage>> TheGameMessageFactory;
typedef Factory<GameMessage> GameMessageFactory;
typedef Singleton<GameMessageFactory> TheGameMessageFactory;