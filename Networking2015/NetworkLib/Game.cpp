#include <assert.h>
#include <iostream>
#include "Game.h"
#include "Screen.h"
#include "Timer.h"
#include "EventPoller.h"
#include "CollisionSystem.h"


int FPS() // measures frame rate
{
  static int numFrameSoFar = 0;
  static float timeSoFar = 0;
  static int fps = 0;

  float dt = Timer::Instance()->GetDt();
  timeSoFar += dt;

  if (timeSoFar >= 1.0f)
  {
    fps =numFrameSoFar;

    numFrameSoFar = 0;
    timeSoFar -= 0.1f;
  }

  return fps;
}

Game::Game()
{

  m_collisionSystem = 0;

  int numJs = SDL_NumJoysticks();

  for (int i = 0; i < numJs; i++)
  {
    SDL_JoystickOpen(i);
  }
  SDL_JoystickEventState(SDL_ENABLE);

  m_state = 0;
}


void Game::RunGame()
{
  while(true)
  {
    Update();
    Draw();
    TheScreen::Instance()->Flip();
  }
}

void Game::Set(GameState* state)
{
  if (m_state)
  {
    m_state->OnInActive();
  }

  m_state = state;
  m_state->OnActive();
}

void Game::Update()
{
  Timer::Instance()->Update();
  TheEventPoller::Instance()->Update();

  assert(m_state);
  m_state->Update();

}

void Game::UpdateCollisions()
{
  assert(m_collisionSystem);
  m_collisionSystem->Update(&m_gameObjects);
}

void Game::Draw()
{
  assert(m_state);
  m_state->Draw();
}

void Game::DrawGameObjects()
{
  for (GameObjectMap::iterator it = m_gameObjects.begin();
    it != m_gameObjects.end();
    ++it)
  {
    GameObject* g = it->second;
    g->Draw();
  }
}

void Game::UpdateGameObjects()
{

  for (GameObjectMap::iterator it = m_gameObjects.begin();
    it != m_gameObjects.end();
    ++it)
  {
    GameObject* g = it->second;
    g->Update();
  }

}

void Game::AddGameObjects(int id, GameObject* go)
{
  m_gameObjects[id] = go;
}

void Game::SetCollisionSystem(CollisionSystem* cs)
{
  m_collisionSystem = cs;
}

void Game::HandleCollision(GameObject* g1, GameObject* g2)
{
  TypePair tp(
    g1->GetTypeName(), g2->GetTypeName());

  CollisionMap::iterator it =
    m_collMap.find(tp);

  if (it != m_collMap.end())  
  {
    CollisionHandler ch = it->second;
    ch(g1, g2);
    return;
  }

  std::swap(tp.first, tp.second); // checks both variations

  it = m_collMap.find(tp);

  if (it != m_collMap.end())
  {
    CollisionHandler ch = it->second;
    ch(g2,g1); // swapped parameters
    return;
  }
}

bool Game::AddCollisionHandler(
  const std::string& typeName1,
  const std::string& typeName2,
  CollisionHandler ch)
{
  TypePair tp(typeName1, typeName2);
  m_collMap[tp] = ch;
  return true;
}


GameObject* Game::GetGameObject(int id)
{
  GameObjectMap::iterator it = m_gameObjects.find(id);

  if (it == m_gameObjects.end())
  {
    return 0;
  }

  return it->second;
}

void Game::DeleteGameObject(int id)
{
  GameObject* go = GetGameObject(id);
  assert(go);

  //delete go;
  m_gameObjects.erase(id);
}

void Game::DeleteAllGameObjects()
{
  for(GameObjectMap::iterator it = m_gameObjects.begin();
    it != m_gameObjects.end();
    ++it )
  {
    GameObject* go = it->second;
    it = m_gameObjects.erase(it);
    delete go;
  }

  m_gameObjects.clear();
}
