#pragma once

#include "Image.h"

class SpriteSheet : public Image
{

public:
  SpriteSheet();
  bool Load(const std::string& fileName, int numCellsInX, int numCellsInY);
  void Draw(int x, int y, int cellNum);

  int GetCellWidth() const {return m_cellWidth;}

  int GetCellHeight() const {return m_cellHeight;}

private:
  int m_numCellsInX;
  int m_numCellsInY;

  int m_cellHeight;
  int m_cellWidth;
};