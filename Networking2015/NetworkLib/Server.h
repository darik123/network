#pragma once
#include <vector>

#include "Uncopyable.h"
#include "Singleton.h"
#include "Mutex.h"
#include "Socket.h"
#include "ServerSocket.h"

typedef std::vector<Socket*> SocketVector;

class Server;
typedef Singleton<Server> TheServer;
class Server : public Uncopyable
{
public:
  bool Init();
  Socket* GetClient();
  SocketVector GetVector()
  {
    return m_vector;
  }
private:
  Server();
  ~Server() {}

  Socket* m_clientSocket;
  ServerSocket m_servSocket;
  Mutex m_mutex;

  friend TheServer;

protected:
  int m_id;
  SocketVector m_vector;
};