#pragma once

#include <string>
#include <fstream>
#include <vector>

class File
{
public:
  bool OpenRead( 
    const std::string& filename);
  bool GetInteger(int* result);
  bool GetFloat(float* result);
  bool GetString(std::string* result);



private:
  std::ifstream m_file;
};