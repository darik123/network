#include "Screen.h"

Screen::Screen()
{
  m_screen = 0;
}

bool Screen::Init(int w, int h, bool fullScreen)
{

  SDL_Init(SDL_INIT_EVERYTHING);
  m_screen = SDL_SetVideoMode(w, h, 0, SDL_SWSURFACE); // SDL_SWSURFACE or SDL_FULLSCREEN

  return (m_screen != 0); // shorter wait than writing an ifelse  statment. If m_screen is == 0 return false if not return true.

}

void Screen::Flip()
{
  SDL_Flip(m_screen);
  //SDL_FillRect(m_screen, NULL, 0x000000);
}

SDL_Surface* Screen::GetSurface()
{
  return m_screen;
}
