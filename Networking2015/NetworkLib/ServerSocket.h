#pragma once

#include "Socket.h"
#include "ClientSocket.h"

class ServerSocket : public Socket
{
public:
  ~ServerSocket() {}
  ServerSocket() {}
  Socket* Accept();
  //void Accept();
  bool ServerOpen(int port);
protected:
};