#pragma once
#include <assert.h>
#include <vector>
#include <string>
#include "Vec2.h"

class SocketMessage
{
public:
  SocketMessage();
  void Clear();
  const char* GetData() const;
  int GetLength() const;
  void AppendData(char* data, char* end);

  bool ReadFloat(float* f);
  bool ReadInt(int* i);
  bool ReadString(std::string* s);

  void WriteFloat(float f);
  void WriteInt(int i);
  void WriteString(const std::string& s);

private:
  std::vector<char> m_data;
  int m_readIndex;
};