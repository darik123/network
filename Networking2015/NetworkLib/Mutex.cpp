#include "Mutex.h"

Mutex::Mutex()
{
  m_mutex = SDL_CreateMutex();
}
Mutex::~Mutex()
{
  SDL_DestroyMutex(m_mutex);
}
void Mutex::Lock()
{
  SDL_LockMutex(m_mutex);
}
void Mutex::Unlock()
{
  SDL_UnlockMutex(m_mutex);
}
///////////////////////
MutexLock::MutexLock(Mutex* mutex)
{
  m_mutex = mutex;
  mutex->Lock();
}
MutexLock::~MutexLock()
{
  m_mutex->Unlock();
}