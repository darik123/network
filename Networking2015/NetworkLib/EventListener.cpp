#include "EventListener.h"

void EventListener::ListenerEvent(const SDL_Event& event)
{
  switch(event.type)
  {
  case SDL_KEYDOWN:
  case SDL_KEYUP:
    OnKeyboardEvent(event.key);
    break;

  case SDL_MOUSEMOTION:
    OnMouseMotionEvent(event.motion);
    break;

  case SDL_JOYAXISMOTION:
    OnJoyAxisEvent(event.jaxis);
    break;

  }
}
