#pragma once

#include <SDL.h>
#include "Singleton.h" // includes template
#include "Vec2.h"

class Screen;
typedef Singleton<Screen> TheScreen;

class Screen : public Uncopyable
{
private:
  Screen();
  friend class Singleton<Screen>;

public:
  bool Init(int w, int h, bool fullScreen);
  void Flip();
  SDL_Surface* GetSurface();
  Vec2i ScreenSize();

private:
  SDL_Surface* m_screen;
};