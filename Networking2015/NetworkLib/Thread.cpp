#include "Thread.h"
#include <assert.h>
#include <SDL_net.h>

//void Thread::Start()
//{
//	m_Thread = SDL_CreateThread(ThreadStarter,this);
//	//delete this;
//}
//int Thread::ThreadStarter(void* data)
//{
//	((Thread*)data)->Work();
//	delete (Thread*)data;
//	return 0;
//}


Thread::Thread()
{
  static int uniqueId = 0;
  m_id = uniqueId++;
}
static int ThreadFunc(void* data)
{
  Thread* thread = reinterpret_cast<Thread*>(data);
  assert(dynamic_cast<Thread*>(thread));

  thread->Work();
  
  return 0;
}
void Thread::Start()
{
  SDL_CreateThread(ThreadFunc, this);
}
