#pragma once

#include <set>
#include "EventListener.h"
#include "Singleton.h"

//singleton design

class EventPoller : public Uncopyable
{
private:
  EventPoller(){}
  friend class Singleton<EventPoller>;
public:
  void Update();
  void AddListener(EventListener*);
  void RemoveListener(EventListener*);

private:
  typedef std::set<EventListener*> Listeners;
  Listeners m_listeners;
};

typedef Singleton<EventPoller> TheEventPoller;