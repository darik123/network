#include "GMGetTurn.h"
#include "GameMessage.h"
#include "GameMessageFactory.h"


#include <iostream>


GameMessage* createGMGetTurn(){
  return new GMGetTurn;
}

static bool n = TheGameMessageFactory::Instance()->Add(GMGetTurn::StaticTypeName(), createGMGetTurn);


GMGetTurn::GMGetTurn()
{
  m_id = GetTypeName();
 // m_timeStamp = 0.0f;
  m_currentTurn = 0.0f;
}

void GMGetTurn::Execute(){
  std::cout << m_currentTurn << std::endl;
}

void GMGetTurn::WriteSocketMessage(SocketMessage* socketMessage)
{
  socketMessage->WriteString(m_id);
  //socketMessage->WriteFloat(m_timeStamp);
  socketMessage->WriteInt(1);//turn 1
}

bool GMGetTurn::ReadSocketMessage(SocketMessage* socketMessage)
{
  int turn = 0;
  if (!socketMessage->ReadInt(&turn))
  {
    //ReportError("failed to readInt socket message");
    return false;
  }
  m_currentTurn = turn;

  return true;
}

const char* GMGetTurn::StaticTypeName()
{
  return "GMGetTurn";
}

const char* GMGetTurn::GetTypeName()const 
{
  return StaticTypeName();
}