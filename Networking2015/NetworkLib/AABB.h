#pragma once

template <class T > class AABB;

typedef AABB<float> AABBf;

template <class T>
class AABB
{
public:
  AABB() : m_minX(0), m_maxX(0), m_minY(0), m_maxY(0) {}

  AABB(T minX, T maxX, T minY, T maxY)
  {
    m_minX = minX;
    m_maxX = maxX;
    m_minY= minY;
    m_maxY = maxY;
  }

  bool Intersects(const AABB& otherBox) const
  {
    return
      (m_minX < otherBox.m_maxX &&
      otherBox.m_minX < m_maxX &&
      m_minY < otherBox.m_maxY &&
      otherBox.m_minY < m_maxY);
  }

private:
  T m_minX;
  T m_maxX;
  T m_minY;
  T m_maxY;
};