#include "SimpleCollisionSystem.h"
#include <iostream>
#include "Game.h"

void RespondToCollision(GameObject* go1, GameObject* go2)
{
  TheGame::Instance()->HandleCollision(go1,go2);
}

void SimpleCollisionSystem::Update(GameObjectMap* Obj)
{
  for (GameObjectMap::iterator it= Obj->begin(); it != Obj->end(); ++it)
  {
    GameObjectMap::iterator jt = it;
    ++jt;

    for ( ; jt != Obj->end(); jt++)
    {
      std::cout << "Checking objects " 
        << it->first
        << " and "
        << jt->first
        << "\n";

      GameObject* go1 = it->second;
      GameObject* go2 = jt->second;
      // check to see if go1 and go2 collide
      if (TheyCollide(go1, go2))
      {
        RespondToCollision(go1,go2);
        break;
      }
    }
  }
}