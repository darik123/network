#include "Socket.h"
#include <assert.h>

#define SDL_reinterpret_cast(type, expresssion) reinterpret_cast<type>(expresssion)

Socket::Socket(TCPsocket sock)
{
  m_sockSet = SDLNet_AllocSocketSet(1);
  m_tcpSock = sock;
  SDLNet_TCP_AddSocket(m_sockSet, m_tcpSock);
}

Socket::Socket()
{
  m_sockSet = SDLNet_AllocSocketSet(1);
  SDLNet_TCP_AddSocket(m_sockSet, m_tcpSock);
}

Socket::~Socket()
{
  Close();
}

bool Socket::HasData() const
{
  int numReady = SDLNet_CheckSockets(m_sockSet, 0);
  assert(numReady == 1 || numReady == 0);
  ////return (numReady == 1);
  return (numReady > 0);
  // return SDLNet_CheckSockets(m_sockSet, 0) != 0;
}

bool Socket::Receive(SocketMessage* sm){
  const int MAXLEN = 2000;
  char tempBuffer[MAXLEN + 1];

  while (HasData()){
    int lengthReceived = SDLNet_TCP_Recv(m_tcpSock, tempBuffer, MAXLEN);
    if (lengthReceived > 0)
    {
      sm->AppendData(tempBuffer, tempBuffer + lengthReceived);
    }
    else {
      return false;
    }
  }
  return true;
}

bool Socket::Send(const SocketMessage& message)
{
  /*const void**/
  assert(m_tcpSock);
  const char* data = message.GetData();
  int len = message.GetLength();

  int result = (SDLNet_TCP_Send(m_tcpSock, data, len));

  if(result != len)
  {
    std::cout << "Failed! No response from host" << "\n";
    std::cout << SDLNet_GetError();
    return false;
  }
  return true;
}

void Socket::Close()
{
  if(m_tcpSock && m_sockSet)
  {
    SDLNet_TCP_DelSocket(m_sockSet, m_tcpSock);

    SDLNet_TCP_Close(m_tcpSock);

    SDLNet_FreeSocketSet(m_sockSet);
  }
}