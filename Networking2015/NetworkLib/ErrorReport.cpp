#include "ErrorReport.h"

#ifdef WIN32
#include <Windows.h>
#endif

void ErrorReport(const std::string& errorMsg)
{
#ifdef WIN32
  MessageBoxA(0, errorMsg.c_str(), "Oh No!!", MB_ICONSTOP);
#endif

}