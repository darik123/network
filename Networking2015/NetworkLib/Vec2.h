#pragma once

template <class T>
struct Vec2
{
  T x, y;

  Vec2() : x(0), y(0) // member initialisation list 
  {
    x = 0;
    y = 0;
  }

  Vec2(T px, T py) : x(px), y(py)
  {
    x = px;
    y = py;
  }

  Vec2& operator+=(const Vec2& v)
  {
    x += v.x;
    y += v.y;
    return *this;
  }

   Vec2& operator-=(const Vec2& v)
  {
    x -= v.x;
    y -= v.y;
    return *this;
  }

  Vec2 operator+(const Vec2& a) const
  {
    Vec2 result = *this;
    result += a;
    return result;
  }

   Vec2 operator-(const Vec2& a) const
  {
    Vec2 result = *this;
    result -= a;
    return result;
  }

  Vec2& operator*=(float f)
  {
    x *= f;
    y *= f;
    return *this;
  }

  Vec2 operator*(float f) const
  {
    Vec2 result = *this;
    result *= f;
    return result;
  }

  bool operator==(const Vec2& a) const
  {
	return(x == a.x && y == a.y);
  }


};

//template <class T>
//Vec2<T> operator+(const Vec2<T>& a, const Vec2<T>& b)
//{
//  Vec2<T> result = a;
//  result += b;
//  return result;
//}

typedef Vec2<int> Vec2i;
typedef Vec2<float> Vec2f;




