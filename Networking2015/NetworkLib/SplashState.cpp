#include "SplashState.h"
#include "Game.h"
//#include "PlayState.h"


bool SplashState::Load()
{
  if (!m_image.Load("../Assets/background.png"))
  {
    return false;
  }
  //load anything else

  return true; // final line
}

void SplashState::Update()
{
}

void SplashState::Draw()
{
  m_image.Draw(0,0);
}

void SplashState::OnKeyboardEvent(const SDL_KeyboardEvent& k)
{
  if (k.keysym.sym == SDLK_SPACE && k.state == SDL_RELEASED)
  {
    //TheGame::Instance()->Set(ThePlayState::Instance());
  }
}

void SplashState::OnActive()
{
  GameState::OnActive();
  static bool Sneaky = Load();
}

void SplashState::OnInActive()
{
  GameState::OnInActive();
}