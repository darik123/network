#pragma once

#include <SDL_net.h>
#include "SocketMessage.h"

#define SDL_reinterpret_cast(type, expresssion) reinterpret_cast<type>(expresssion)


SocketMessage::SocketMessage()
{
  m_readIndex = 0;
}
void SocketMessage::Clear()
{
  m_data.clear();

}
const char* SocketMessage::GetData()const
{
  return m_data.data();

}
int SocketMessage::GetLength()const
{
  return m_data.size();
}

void SocketMessage::AppendData(char* data, char* end)
{
  m_data.insert(m_data.end(), data, end);
}

bool SocketMessage::ReadFloat(float* f){
  int size = m_data.size();
  assert(m_readIndex <= size);

  int bytes = size - m_readIndex;
  assert(bytes >= sizeof(float));

  if (bytes < sizeof(float))
  {
    return false;
  }

  float* k = reinterpret_cast<float*>(m_data.data() + m_readIndex);

  *f = *k;

  m_readIndex += sizeof(float);
  return true;
}

bool SocketMessage::ReadInt(int* i)
{
  if (m_readIndex + sizeof(int) > m_data.size())
  {
    return false;
  }
  int nextIndex = m_readIndex;
  m_readIndex += sizeof(int);

  *i = SDLNet_Read32(&m_data[nextIndex]);
  return true;
}

bool SocketMessage::ReadString(std::string* s)
{
  int stringSize = 0;
  if(!ReadInt(&stringSize))
  {
    return false;
  }

  if((m_readIndex + stringSize) > m_data.size())
  {
    return false;
  }

  char* d = &(m_data[m_readIndex]);
  assert(d);

  s->append(d, d + stringSize);

  m_readIndex += stringSize;

  return true;
}

void SocketMessage::WriteFloat(float f)
{
  SDLNet_Write32(f, &f);
  char* c = reinterpret_cast<char*>(&f);
  AppendData(c, c + sizeof(float));
}

void SocketMessage::WriteInt(int i)
{
  SDLNet_Write32(i, &i);
  char* c = reinterpret_cast<char*>(&i);
  AppendData(c, c + sizeof(int));
}

void SocketMessage::WriteString(const std::string& s)
{
  int len = s.size();
  WriteInt(len);
  char* c = (char*)s.c_str();
  AppendData(c, c + len);
}