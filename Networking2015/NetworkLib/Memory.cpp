#ifdef MEM_DEBUG

#include <stdlib.h>
#include "Memory.h"

#undef new

static int depth = 0;
static size_t s_totalAllocated;
static const size_t MAX_HEAP = 10000000;

void* operator new(size_t sizeOfBytes)
{
  return operator new(sizeOfBytes, "<unknown file name>", 0);
}

void* operator new(size_t sizeOfBytes, 
  const char* filename, 
  int lineNum)
{
  void* mem = malloc(sizeOfBytes);

  depth++;
  if(depth == 1)
  {
    Allocation a(sizeOfBytes, mem, filename, lineNum);
    TheAllocMan::Instance()->AddAlloc(a);
  }
  depth--;

  return mem;
}

void operator delete(void* p)
{
  if(p != NULL)
  {
    depth++;
    if(depth == 1)
    {
      TheAllocMan::Instance()->RemoveAlloc(p);
    }
    depth--;

    free(p);
  }
}

// Use this non-member function to override the operator << 
std::ostream& operator<<(std::ostream& os, const Allocation& alloc)
{
   return os << "Leak! Size: " << alloc.m_size << ", address: " << 
     alloc.m_addr << " Filename: " << alloc.m_filename << "Line num: "
     << alloc.m_lineNum << "\n";
}

AllocMan::~AllocMan()
{
  depth++; // prevents any further access to AllocMan
  ReportLeaks();
}

void AllocMan::AddAlloc(const Allocation& a)
{
  m_map[a.m_addr] = a;
}

void AllocMan::RemoveAlloc(void* addr)
{
  m_map.erase(addr);
}

void AllocMan::ReportLeaks()
{
  std::ofstream of;
  of.open("leaks.txt");
  for(AllocMap::iterator it = m_map.begin(); it != m_map.end(); ++it)
  {
    std::cout << it->second;
    of << it->second;
  }
}

#endif