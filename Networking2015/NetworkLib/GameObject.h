#pragma once

#include <map>
#include "SmartPointer.h"
#include "AABB.h"
#include "File.h"
#include "Vec2.h"
#include "Sprite.h"

class GameObject : public RefCounted
{
public:
  virtual ~GameObject() {}

  virtual void Draw() = 0;
  virtual void Update() = 0;

  virtual const char* GetTypeName() const = 0;

  const AABBf& GetAABB() const {return m_aabb;}

  virtual bool Load(File*)
  {
    return true;
  }

  void SetId(int id);
  int GetID() const;

  void SetPos(const Vec2f& pos);
  Vec2f GetPos() const {return m_pos;}

protected:
  AABBf m_aabb;
  int m_id;
  Vec2f m_pos;
  Vec2f m_vel;
};


// CHANGED CODE!!!!!!!!!!!!!!!!!!! SWITCH TO TEST

// Smart Pointer
typedef SmartPointer<GameObject> pGameObject;

//Raw Pointer
//typedef GameObject* pGameObject;

typedef std::map<int, pGameObject> GameObjectMap;