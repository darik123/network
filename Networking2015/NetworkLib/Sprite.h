#pragma once

#include "AABB.h"
#include "Vec2.h"
#include "SpriteSheet.h"

class Sprite
{
public:
  Sprite();

  void Draw();
  void Update();

  void SetSprite(SpriteSheet* ss, float maxElaspedTime);

  Vec2f Center(); // gets the offset to center

  void SetPos(const Vec2f& pos) {m_pos = pos;}
  void SetVel(const Vec2f& vel) {m_vel = vel;}
  void SetAcc(const Vec2f& acc) {m_acc = acc;}

  Vec2f GetPos() const {return m_pos;}
  Vec2f GetVel() const {return m_vel;}
  Vec2f GetAcc() const {return m_acc;}

  AABBf GetAABB() const;


private:
  float m_elapsedTime;
  float m_maxElapsedTime;
  unsigned int m_cellNum;
  SpriteSheet* m_spriteSheet;
  Vec2f m_pos; // position
  Vec2f m_vel; // velocity
  Vec2f m_acc; // acceleration
};