#pragma once

#include "Socket.h"

class ClientSocket : public Socket
{
public:
  ClientSocket();
  ClientSocket(TCPsocket socket) {}
  bool ClientOpen(const std::string& hostname, int port);
};