#pragma once

#include <SDL_thread.h>
#include <SDL_mutex.h>

class Thread
{
public:
  Thread();
  void Start();
  virtual void Work() = 0;
  virtual ~Thread() {}
private:
  int m_id;
  SDL_Thread* m_thread;
  SDL_mutex* m_mutex;
};

//class Thread
//{
//public:
//	void Start();
//private:
//	virtual void Work() = 0;
//	SDL_Thread * m_Thread;
//	static int ThreadStarter(void* data);
//};