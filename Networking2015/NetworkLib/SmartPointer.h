#pragma once

class RefCounted
{
protected:
  RefCounted() : m_refCount(0) {}

  template <class T>
  friend class SmartPointer;

private:
  int m_refCount;
};

template <class T>
class SmartPointer
{
public:
  SmartPointer(T* ptr = 0) : m_ptr(ptr)
  {
    if(m_ptr)
    {
      m_ptr->m_refCount++;
    }
  }

  // Need to pass by reference
  SmartPointer(const SmartPointer& other)
  {
    m_ptr = other.m_ptr;
    if(m_ptr)
    {
      m_ptr->m_refCount++;
    }
  }

  ~SmartPointer() 
  { 
    if(m_ptr)
    {
      m_ptr->m_refCount--;
      if(m_ptr->m_refCount == 0)
      {
        delete m_ptr;
      }
    }
  }

  // Assignment operator
  SmartPointer& operator=(const SmartPointer& other)
  {
    if(m_ptr == other.m_ptr)
    {
      return *this;
    }

    // Decrement the ref count of m_ptr
    // (check for non zero!)
    if(m_ptr)
    {
      m_ptr->m_refCount--;
      if(m_ptr->m_refCount == 0)
      {
        delete m_ptr;
      }
    }
    // Copy the other pointer
    m_ptr = other.m_ptr;

    // Inc ref count
    // (check for non zero!)
    if(m_ptr) // not the same address
    {
      m_ptr->m_refCount++;
    }

    return *this;
  }

  T* operator->() const { return m_ptr; }

  operator T* () const { return m_ptr; }

private:
  T* m_ptr;
  RefCounted m_rc;
};