#pragma once

#include <SDL.h>
#include "Uncopyable.h"

class Mutex : public Uncopyable
{
public:
  Mutex();
  ~Mutex();

  void Lock();
  void Unlock();
private:
  SDL_mutex* m_mutex;
};

class MutexLock : public Mutex
{
public:
  MutexLock(Mutex* mutex);
  ~MutexLock();
private:
  Mutex* m_mutex;
};