#include "MessageQueue.h"
#include "Mutex.h"

void GameMessageQueue::AddMessage(GameMessage* gm)
{
  MutexLock lock(&m_mutex);
  m_GMQueue.push(gm);
  lock.Unlock();
}

void GameMessageQueue::Update()
{
  MutexLock lock(&m_mutex);
  while (!m_GMQueue.empty())
  {
    //we can add maximun lag time and leave all message with less timeStamp for next frame 
    m_GMQueue.top()->Execute();
    delete m_GMQueue.top();
    m_GMQueue.pop();
  }	
  lock.Unlock();
}

void GameMessageQueue::Clear()
{
  MutexLock lock(&m_mutex);
  while (!m_GMQueue.empty())
  {
    delete m_GMQueue.top();
    m_GMQueue.pop();
  }
  lock.Unlock();
}


//bool MessageQueue::GmLessThan::operator ()(GameMessage* g1,GameMessage* g2)
//{
//        return (g1->GetTimeStamp() < g2->GetTimeStamp());
//}
//
//void MessageQueue::Update()
//{
//        while(true)
//        {
//                for(int i = 0; i < m_MsgQueue.size(); i++)
//                {
//                        // check mutex is avaliable
//                  MutexLock lock(&m_mutex);
//                        m_MsgQueue.top()->Execute();
//                        m_MsgQueue.pop();
//                }
//        }
//}
//
//void MessageQueue::AddToQueue(GameMessage* Gmsg)
//{
//  MutexLock lock(&m_mutex);
//        m_MsgQueue.push(Gmsg);
//}
//
//void MessageQueue::Work()
//{
//        Update();
//}