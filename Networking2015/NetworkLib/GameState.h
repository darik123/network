#pragma once

#include "EventListener.h"
#include "Uncopyable.h"

class GameState : public EventListener,
  public Uncopyable
{
public:
  virtual ~GameState() {}

  virtual void Draw() = 0;
  virtual void Update() = 0;

  // if you override this, call base class version!
  virtual void OnActive();

  virtual void OnInActive();
};