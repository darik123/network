#include "ClientSocket.h"

#define SDL_reinterpret_cast(type, expression) reinterpret_cast<type>(expression)

ClientSocket::ClientSocket()
{

}

bool ClientSocket::ClientOpen(const std::string& hostname, int port)
{
  IPaddress ip;
  if(SDLNet_ResolveHost(&ip, hostname.c_str(), port) != 0)
  {
    std::cout << "Look up failed, error: " << SDLNet_GetError() << "\n";
    return false;
  }

  //SDLNet_ResolveHost(&ip, hostname.c_str(), port);

  //(&ip, hostname.c_str(), port);

  unsigned char* ipArray = NULL;
  ipArray = (unsigned char*)(&ip.host);

  if(ip.host == INADDR_NONE)
  {
    return false;
  }

  m_tcpSock = SDLNet_TCP_Open(&ip);

  if(!m_tcpSock)
  {
    std::cout << "Oh, no!" << "\n";
    return false;
  }


  m_sockSet = SDLNet_AllocSocketSet(1);

  if(m_tcpSock)
  {
    /*int result = SDLNet_TCP_AddSocket(m_sockSet, m_tcpSock);

    if(result == 0)
    {
    return false;
    }*/
    SDLNet_TCP_AddSocket(m_sockSet, m_tcpSock);
  }

  std::cout << "Succesfully Connected" << "\n";
  std::cout << "IP: " << (int)ipArray[0] << "." << (int)ipArray[1] << "." << (int)ipArray[2] << "." << (int)ipArray[3] << "\n";

  return true;
}