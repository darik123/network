#include <iostream>
#include <map>
#include <SDL.h>
#include <SDL_net.h>
#include <ServerSocket.h>
#include <Server.h>
#include <GameMessage.h>
#include <Thread.h>
#include <Mutex.h>
#include <GMGetTurn.h>
#include <SendRecv.h>
#include <AnMsg.h>
#include <MsgRcVThread.h>
#include <MessageQueue.h>
#include "ServerThreads.h"


#define SDL_reinterpret_cast(type, expresssion) reinterpret_cast<type>(expresssion)

// Listen Socket
ServerSocket server;
// Vec of connected Sockets
std::vector<Socket> vec;

#undef main

int main(int argc, char* argv[])
{ 
  SDL_Init(0);
  SDLNet_Init();

  std::string port;
  std::cout << "*********Welcome to the Game*********\n" 
    << " ########You are the Server!########\n\n"
    << "Type the portNum:\n";
  std::getline(std::cin, port);
  int portNum = atoi(port.c_str());

  if(!server.ServerOpen(portNum))
  {
    return false;
  }
  else
  {
    std::cout << "Server waiting for connection on " << portNum << "\n";
    while(vec.empty() && vec.size() < 1)
    {
      Socket* s = server.Accept();
      vec.push_back(*s);
      std::cout << "Client Connected" << "\n";
    }

    /*ServMsgRcVThread* mrt = new ServMsgRcVThread(&vec[0]);
    mrt->Start();*/
    
    ServerWriterThread swT(&vec[0]);
    swT.Start();

    ServerReaderThread srT(&vec[0]);
    srT.Start();
  }

 

  while(true)
  {
   // GMGetTurn* gm = new GMGetTurn;
   // SendGameMessage(&vec[0], gm);
  //  gm->Execute();
    int i = 0;
    i++;
  }

  vec[0].Close();
  SDL_Quit();

  return 0;
}