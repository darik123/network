#pragma once

#include <Thread.h>
#include <Mutex.h>
#include <SocketMessage.h>
#include <ServerMessageQueue.h>
#include <Socket.h>
#include <MsgRcVThread.h>

class ServerReaderThread : public Thread
{
public:
  ServerReaderThread() {}
  ServerReaderThread(Socket* s) : m_socket(s) {}
  void Work()
  {
    m_mutex.Lock();
    int quit = 0;
    while(!quit)
    {
      SocketMessage sm;
      std::string message;
      m_socket->Receive(&sm);
      if(sm.ReadString(&message))
      {
        std::cout << "Client says: " << "\n" << message << "\n";
        if(message == "quit")
        {
          std::cout << "Client Closed the connection" << "\n";
          m_socket->Close();
          SDL_Quit();
          quit = 1;
          break;
        }
      }
      // mutex.Unlock();
    }
  }
private:
  Socket* m_socket;
  Mutex m_mutex;
};

class ServerWriterThread : public Thread
{
public:
  ServerWriterThread(Socket* sock) : m_socket(sock) {}
  void Work()
  {
    while(true)
    {
      m_mutex.Lock();
      std::cout << "Write Server Message: " << "\n";
      std::string message;
      std::getline(std::cin, message);// Get line of text
      SocketMessage sm;
      sm.WriteString(message);
      m_socket->Send(sm);
      //mutex.Unlock();
    }
  }
private:
  Socket* m_socket;
  Mutex m_mutex;
};

//class ServerAcceptThread : public Thread
//{
//public:
//  ServerAcceptThread(ServerSocket* sock) : m_socket(sock) {}
//
//  void Work()
//  {
//    //ServerSocket serverSock;
//    m_socket->ServerOpen(777);
//    {
//      while (true)
//      {
//        m_mutex.Lock();
//        //Socket* s = serverSock.Accept();
//        m_socket->Accept();
//
//        int i = 0;
//        i++;
//
//        /* mutex.Lock();
//        vec.push_back(*s);
//        std::cout <<  "Server got a new client request...\n";
//        mutex.Unlock();
//
//        SDL_Delay(3000);
//        ServerReaderThread d(&vec.at(0));
//        d.Start();
//        */
//        //SDL_Delay(3000);
//
//        /*ServerWriterThread td(&vec[0]);
//        td.Start();
//
//        ServerReaderThread d(&vec[0]); 
//        d.Start();*/
//
//        //SDL_Delay(3000);
//      }
//    }
//  }
//private:
//  Mutex m_mutex;
//  ServerSocket* m_socket;
//};
//
////class ServerMsgThread : public Thread
////{
////public:
////  ServerMsgThread(Socket* sock) : m_socket(sock) {}
////  void Work()
////  {
////    // static float f = 0.0f;
////    while(true)
////    {
////
////      // SDL_Delay(3000);
////      //lock.Lock();
////      //system("PAUSE");
////      // mutex.Lock();
////      // system("PAUSE");
////      //GameMessage* gm = new PlayerTurn();
////      //gm->setTimeStamp(f);
////      //// gm->Execute();
////      ////gm->SetType(gm->GetName());
////      ////gm->setTimeStamp(1.0f);
////      //if(sendGameMessage(&vec[0], gm))
////      //{
////      //f += 0.0001f;
////      //}
////      ////mutex.Unlock();
////      SocketMessage sm;
////      sm.writeString("AnMsg");
////      sm.writeString("message");
////      //gm->Execute();
////      //SendGameMessage(*gm,&vec[0]);
////      vec[0].Send(sm);
////    }
////  }
////private:
////  Socket* m_socket;
////};
//
//class ThreadServer : public Thread
//{
//private:
//  typedef std::vector<Socket> ClientVec;
//public:
//  ThreadServer(ServerSocket* sock, ClientVec cv) : m_servSocket(sock) {}
//  void Work()
//  {
//    while(true)
//    {
//      if(m_cVec.empty())
//      {
//        Socket* cs = m_servSocket->Accept();
//        //m_mutex.Lock();
//        //lock.Lock();
//        m_cVec.push_back(*cs);
//        std::cout << "connected" << "\n";
//        //m_mutex.Unlock();
//      }
//      if(!m_cVec.empty())
//      {
//        bool as = true;
//        if(as)
//        {
//          SDL_Delay(3000);
//          ServerWriterThread a(&m_cVec[0]);
//          std::cout << "start thread" << "\n";
//          a.Start();
//          as = false;
//        }
//      }
//      else
//        break;
//    }
//  }
//private:
//  ServerSocket* m_servSocket;
//  Mutex m_mutex;
//  ClientVec m_cVec;
//};

class ServMsgRcVThread : public MsgRcVThread
{
public:
  ServMsgRcVThread(Socket* sock) : m_socket(sock) {}

  void Work()
  {
    while(true)
    {
      m_mutex.Lock();
      GameMessage* gm = new GMGetTurn;
      
      if(gm = ReceiveGameMessage(m_socket))
      {
        TheGameMessageQueue::Instance()->AddMessage(gm);
        TheGameMessageQueue::Instance()->Update();
      }
    }
  }
private:
  Socket* m_socket;
  Mutex m_mutex;
};