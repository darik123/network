#pragma once

#include <Thread.h>
#include <Socket.h>
#include <MsgRcVThread.h>
#include <MessageQueue.h>

class WriterThread : public Thread
{
public:
  WriterThread(Socket* sock) : m_socket(sock) {}
  void Work()
  {
    while(true)
    {
      m_mutex.Lock();
      std::string message;
      std::cout << "Write message to Server: " << "\n";
      std::getline(std::cin, message); // Get line of text

      SocketMessage sm;
      sm.WriteString(message);
      m_socket->Send(sm);
      // lock.Unlock();
      if(message == "quit")
      {
        break;
      }
    }
  }

private:
  Socket* m_socket;
  Mutex m_mutex;
};

class ReaderThread : public Thread
{
public:
  ReaderThread(Socket* sock) : m_socket(sock) {}
  void Work()
  {
    while(true)
    {
      m_mutex.Lock();
      SocketMessage sm;
      std::string message;
      //mutex.Lock();
      m_socket->Receive(&sm);
      //lock.Lock();
      // vecSM.push_back(sm);
      //lock.Unlock();
      if(sm.ReadString(&message))
      {
        std::cout << "Server says: " << "\n" << message << "\n";
      /*  GameMessage* gm = TheGameMessageFactory::Instance()->Create(message);
        gm->SetType(message);
        gm->Execute();*/
        //mutex.Lock();
        //mess.push_back(sm);
        //mutex.Unlock();
      }
    }
    // mutex.Unlock();
  }
private:
  Socket* m_socket;
  Mutex m_mutex;
};