#include <iostream>
#include <vector>
#include <SDL.h>
#include <SDL_net.h>
#include <ClientSocket.h>
#include <Client.h>
#include <Game.h>
#include <Screen.h>
#include <SimpleCollisionSystem.h>
#include <SplashState.h>
#include <Thread.h>
#include <ServerSocket.h>
#include <GMGetTurn.h>
#include <Mutex.h>
#include <GameMessageFactory.h>
#include <MessageQueue.h>
#include <MsgRcVThread.h>
#include "ClientThreads.h"

//#include <SendRecv.h>

std::vector<GameMessage*> vecGM;
std::vector<SocketMessage> mess;

//class ClientMessage : public Thread
//{
//public:
//  ClientMessage(Socket* sock) : m_socket(sock) {}
//  void Work()
//  {
//    while(true)
//    {
//    SocketMessage sm;
//    m_socket->Receive(&sm);
//            std::string gmType;
//
//            int i;
//            sm.readInt(&i);
//            sm.readString(&gmType);
//
//            GameMessage* gm= TheGameMessageFactory::Instance()->Create(gmType);
//
//            if(gm)
//            {
//              gm->Read(&mess[i]);
//              gm->Execute();
//              delete gm;
//            }
//            else
//            {
//              std::cout << gmType;
//            }
//        //mess.clear();
//        //m_mutex.Unlock();
//
//        //lock.Unlock();
//      }
//  }
//private:
//  Socket* m_socket;
//  std::vector<SocketMessage> m_mess;
//  Mutex m_mutex;
//};


#undef main

void Init()
{
  //SDLNet_Init();
  ////Client* client = TheClient::Instance();
  Screen* theScreen = TheScreen::Instance();
  Game* theGame = TheGame::Instance();
  SplashState* splash = TheSplashState::Instance();
  //ClientSocket cs;
  //cs.ClientOpen("127.0.0.1", 123);
  ////client->ServerConnect();
  theScreen->Init(640, 480, false);
  theGame->Set(splash);
  theGame->RunGame();
  //cs.Close();
}
int main(int argc, char* argv[])
{
  SDL_Init(0);
  SDLNet_Init();

  std::string port;
  std::string ip;

  std::cout << "*********Welcome to the Game*********\n" 
    << " ########You are the Client!########\n\n"
    << "Type the IP:\n";

  std::getline(std::cin, ip);

  std::cout << "Type the portNum:\n";
  std::getline(std::cin, port);
  int portNum = atoi(port.c_str());

  ClientSocket cs;
  if(!cs.ClientOpen(ip, portNum))
  {
    return false;
  }
  else
  {
    std::cout << "connected" << "\n";

    //SDL_Delay();

    WriterThread wt(&cs);
    wt.Start();

    ReaderThread rt(&cs);
    rt.Start();

    /*GameMessageQueue* gmQ = TheGameMessageQueue::Instance();
    gmQ->Start();*/

    //MsgRcVThread* mt = new MsgRcVThread(&cs);
    //mt->Start();
  }


  while(true)
  {
   /* SocketMessage sm;
    std::string s;
    int d = 0;
    sm.ReadString(&s);
    sm.ReadInt(&d);
    GameMessage* gm = TheGameMessageFactory::Instance()->Create(s);
    gm->Execute();*/
    int i = 0;
    i++;
  }

  cs.Close();
  SDL_Quit();
 

  return 0;
}